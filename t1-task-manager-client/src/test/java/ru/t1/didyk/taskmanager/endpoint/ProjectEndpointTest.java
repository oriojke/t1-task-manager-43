package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.endpoint.IProjectEndpoint;
import ru.t1.didyk.taskmanager.api.service.IPropertyService;
import ru.t1.didyk.taskmanager.constant.SharedTestMethods;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.marker.IntegrationCategory;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;
import ru.t1.didyk.taskmanager.service.PropertyService;

import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Before
    public void before() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        request.setName("DEFAULT TEST NAME");
        request.setDescription("DEFAULT TEST DESC");
        projectEndpoint.create(request);
    }

    @Test
    public void changeStatusById() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectListRequest listRequest = new ProjectListRequest(userToken);
        listRequest.setSortType(Sort.BY_NAME);
        @Nullable final ProjectListResponse listResponse = projectEndpoint.listProjects(listRequest);
        List<ProjectDTO> projects = listResponse.getProjects();
        Assert.assertNotNull(projects);

        @Nullable final ProjectDTO project = projects.get(0);
        Assert.assertNotNull(project);
        @NotNull ProjectChangeStatusByIdRequest changeRequest = new ProjectChangeStatusByIdRequest(userToken);
        changeRequest.setId(project.getId());
        changeRequest.setStatus(Status.COMPLETED);
        @Nullable final ProjectChangeStatusByIdResponse changeResponse = projectEndpoint.changeStatusById(changeRequest);
        @Nullable final ProjectDTO newProject = changeResponse.getProject();
        Assert.assertNotNull(newProject);
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectChangeStatusByIndexRequest changeRequest = new ProjectChangeStatusByIndexRequest(userToken);
        changeRequest.setIndex(0);
        changeRequest.setStatus(Status.COMPLETED);
        @Nullable final ProjectChangeStatusByIndexResponse changeResponse = projectEndpoint.changeStatusByIndex(changeRequest);
        @Nullable final ProjectDTO newProject = changeResponse.getProject();
        Assert.assertNotNull(newProject);
    }

    @Test
    public void clearProject() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectClearRequest request = new ProjectClearRequest(userToken);
        @Nullable final ProjectClearResponse response = projectEndpoint.clearProject(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void completeById() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectListRequest listRequest = new ProjectListRequest(userToken);
        listRequest.setSortType(Sort.BY_NAME);
        @Nullable final ProjectListResponse listResponse = projectEndpoint.listProjects(listRequest);
        List<ProjectDTO> projects = listResponse.getProjects();
        Assert.assertNotNull(projects);

        @Nullable final ProjectDTO project = projects.get(0);
        Assert.assertNotNull(project);
        @NotNull ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken);
        request.setId(project.getId());
        @Nullable final ProjectCompleteByIdResponse response = projectEndpoint.completeById(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
        Assert.assertEquals(Status.COMPLETED, newProject.getStatus());
    }

    @Test
    public void completeByIndex() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(userToken);
        request.setIndex(0);
        @Nullable final ProjectCompleteByIndexResponse response = projectEndpoint.completeByIndex(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
        Assert.assertEquals(Status.COMPLETED, newProject.getStatus());
    }

    @Test
    public void create() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        request.setName("TEST NEW");
        request.setDescription("DESC NEW");
        @Nullable final ProjectCreateResponse response = projectEndpoint.create(request);
        @Nullable final ProjectDTO project = response.getProject();
        Assert.assertNotNull(project);
    }

    @Test
    public void listProjects() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectListRequest request = new ProjectListRequest(userToken);
        request.setSortType(Sort.BY_NAME);
        @Nullable final ProjectListResponse response = projectEndpoint.listProjects(request);
        List<ProjectDTO> projects = response.getProjects();

        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(project);
        }

        Assert.assertNotNull(projects);
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void removeById() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectListRequest listRequest = new ProjectListRequest(userToken);
        listRequest.setSortType(Sort.BY_NAME);
        @Nullable final ProjectListResponse listResponse = projectEndpoint.listProjects(listRequest);
        List<ProjectDTO> projects = listResponse.getProjects();
        Assert.assertNotNull(projects);

        @Nullable final ProjectDTO project = projects.get(0);
        Assert.assertNotNull(project);
        @NotNull ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setId(project.getId());
        @Nullable ProjectRemoveByIdResponse response = projectEndpoint.removeById(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
    }

    @Test
    public void removeByIndex() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(userToken);
        request.setIndex(0);
        @Nullable ProjectRemoveByIndexResponse response = projectEndpoint.removeByIndex(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
    }

    @Test
    public void showById() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectListRequest listRequest = new ProjectListRequest(userToken);
        listRequest.setSortType(Sort.BY_NAME);
        @Nullable final ProjectListResponse listResponse = projectEndpoint.listProjects(listRequest);
        List<ProjectDTO> projects = listResponse.getProjects();
        Assert.assertNotNull(projects);

        @Nullable final ProjectDTO project = projects.get(0);
        Assert.assertNotNull(project);
        @NotNull ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setId(project.getId());
        @Nullable ProjectShowByIdResponse response = projectEndpoint.showById(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
    }

    @Test
    public void showByIndex() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(userToken);
        request.setIndex(0);
        @Nullable ProjectShowByIndexResponse response = projectEndpoint.showByIndex(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
    }

    @Test
    public void startById() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectListRequest listRequest = new ProjectListRequest(userToken);
        listRequest.setSortType(Sort.BY_NAME);
        @Nullable final ProjectListResponse listResponse = projectEndpoint.listProjects(listRequest);
        List<ProjectDTO> projects = listResponse.getProjects();
        Assert.assertNotNull(projects);

        @Nullable final ProjectDTO project = projects.get(0);
        Assert.assertNotNull(project);
        @NotNull ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken);
        request.setId(project.getId());
        @Nullable ProjectStartByIdResponse response = projectEndpoint.startById(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
        Assert.assertEquals(Status.IN_PROGRESS, newProject.getStatus());
    }

    @Test
    public void startByIndex() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(userToken);
        request.setIndex(0);
        @Nullable ProjectStartByIndexResponse response = projectEndpoint.startByIndex(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
        Assert.assertEquals(Status.IN_PROGRESS, newProject.getStatus());
    }

    @Test
    public void updateById() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectListRequest listRequest = new ProjectListRequest(userToken);
        listRequest.setSortType(Sort.BY_NAME);
        @Nullable final ProjectListResponse listResponse = projectEndpoint.listProjects(listRequest);
        List<ProjectDTO> projects = listResponse.getProjects();
        Assert.assertNotNull(projects);

        @Nullable final ProjectDTO project = projects.get(0);
        Assert.assertNotNull(project);
        @NotNull ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setId(project.getId());
        request.setName("NEW NAME");
        request.setDescription("NEW DESC");
        @Nullable ProjectUpdateByIdResponse response = projectEndpoint.updateById(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
        Assert.assertEquals("NEW NAME", newProject.getName());
        Assert.assertEquals("NEW DESC", newProject.getDescription());
    }

    @Test
    public void updateByIndex() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(userToken);
        request.setIndex(0);
        request.setName("NEW NAME 2");
        request.setDescription("NEW DESC 2");
        @Nullable ProjectUpdateByIndexResponse response = projectEndpoint.updateByIndex(request);
        @Nullable final ProjectDTO newProject = response.getProject();
        Assert.assertNotNull(newProject);
        Assert.assertEquals("NEW NAME 2", newProject.getName());
        Assert.assertEquals("NEW DESC 2", newProject.getDescription());
    }

}
