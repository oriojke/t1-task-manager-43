package ru.t1.didyk.taskmanager.api.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.UserLoginRequest;
import ru.t1.didyk.taskmanager.dto.request.UserLogoutRequest;
import ru.t1.didyk.taskmanager.dto.request.UserViewProfileRequest;
import ru.t1.didyk.taskmanager.dto.response.UserLoginResponse;
import ru.t1.didyk.taskmanager.dto.response.UserLogoutResponse;
import ru.t1.didyk.taskmanager.dto.response.UserViewProfileResponse;

public interface IAuthEndpointClient {

    @NotNull UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull UserViewProfileResponse profile(@NotNull UserViewProfileRequest request);

}
