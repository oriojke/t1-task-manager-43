package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.component.ISaltProvider;
import ru.t1.didyk.taskmanager.api.endpoint.IConnectionProvider;

public interface IPropertyService extends ISaltProvider, IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getGitBranch();

    @NotNull
    String getCommitId();

    @NotNull
    String getCommitterName();

    @NotNull
    String getCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

}
