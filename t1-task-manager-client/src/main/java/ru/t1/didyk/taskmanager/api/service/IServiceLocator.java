package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpoint getAuthEndpointClient();

    @NotNull
    IDomainEndpoint getDomainEndpointClient();

    @NotNull
    IProjectEndpoint getProjectEndpointClient();

    @NotNull
    ITaskEndpoint getTaskEndpointClient();

    @NotNull
    IUserEndpoint getUserEndpointClient();

    @NotNull ITokenService getTokenService();

}
