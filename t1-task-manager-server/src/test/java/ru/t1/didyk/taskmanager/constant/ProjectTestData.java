package ru.t1.didyk.taskmanager.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.component.ISaltProvider;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;
import ru.t1.didyk.taskmanager.dto.model.SessionDTO;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;
import ru.t1.didyk.taskmanager.service.PropertyService;
import ru.t1.didyk.taskmanager.util.HashUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ProjectTestData {

    @NotNull
    public final static ProjectDTO USER_PROJECT = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER_PROJECT3 = new ProjectDTO();

    @NotNull
    public final static List<ProjectDTO> USER_PROJECT_LIST = new ArrayList<ProjectDTO>();

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT = new ProjectDTO();

    @NotNull
    public final static TaskDTO USER_TASK = new TaskDTO();

    @NotNull
    public final static TaskDTO USER_TASK2 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER_TASK3 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN_TASK = new TaskDTO();

    @NotNull
    public final static List<TaskDTO> USER_TASK_LIST = new ArrayList<TaskDTO>();

    @NotNull
    public final static UserDTO USER1 = new UserDTO();

    @NotNull
    public final static UserDTO USER2 = new UserDTO();

    @NotNull
    public final static ProjectDTO NULL_PROJECT = null;

    @NotNull
    public final static TaskDTO NULL_TASK = null;

    @Nullable
    public final static String NULL_STRING = null;

    @NotNull
    public final static SessionDTO SESSION = new SessionDTO();

    static {
        @Nullable final ISaltProvider propertyService = new PropertyService();

        USER1.setLogin("Test1");
        USER1.setFirstName("Testing");
        USER1.setLastName("First");
        USER1.setRole(Role.USUAL);
        USER1.setEmail("test@email.com");
        USER1.setPasswordHash(HashUtil.salt(propertyService, "password"));

        USER2.setLogin("Test2");
        USER2.setFirstName("Testing");
        USER2.setLastName("Second");
        USER2.setRole(Role.ADMIN);
        USER2.setPasswordHash(HashUtil.salt(propertyService, "password"));

        USER_PROJECT.setUserId(USER1.getId());
        USER_PROJECT.setName("First");
        USER_PROJECT.setStatus(Status.IN_PROGRESS);
        USER_PROJECT.setDescription("project");

        USER_PROJECT2.setUserId(USER1.getId());
        USER_PROJECT2.setName("Second User");
        USER_PROJECT2.setStatus(Status.IN_PROGRESS);
        USER_PROJECT2.setDescription("project");

        USER_PROJECT3.setUserId(USER1.getId());
        USER_PROJECT3.setName("Third Project");
        USER_PROJECT3.setStatus(Status.IN_PROGRESS);
        USER_PROJECT3.setDescription("project");

        for (int i = 0; i < 3; i++) {
            ProjectDTO project = new ProjectDTO();
            project.setUserId(USER1.getId());
            project.setName("Collection Project " + i);
            project.setStatus(Status.NOT_STARTED);
            project.setDescription("Collection Project " + i);
            USER_PROJECT_LIST.add(project);

            TaskDTO task = new TaskDTO();
            task.setUserId(USER1.getId());
            task.setName("Collection Task " + i);
            task.setStatus(Status.NOT_STARTED);
            task.setDescription("Collection Task " + i);
            USER_TASK_LIST.add(task);
        }

        ADMIN_PROJECT.setUserId(USER2.getId());
        ADMIN_PROJECT.setName("Second");
        ADMIN_PROJECT.setStatus(Status.COMPLETED);
        ADMIN_PROJECT.setDescription("project");

        USER_TASK.setUserId(USER1.getId());
        USER_TASK.setName("First");
        USER_TASK.setStatus(Status.IN_PROGRESS);
        USER_TASK.setDescription("task");
        USER_TASK.setProjectId(USER_PROJECT.getId());

        USER_TASK2.setUserId(USER1.getId());
        USER_TASK2.setName("Second");
        USER_TASK2.setStatus(Status.IN_PROGRESS);
        USER_TASK2.setDescription("task");

        USER_TASK3.setUserId(USER1.getId());
        USER_TASK3.setName("Third");
        USER_TASK3.setStatus(Status.IN_PROGRESS);
        USER_TASK3.setDescription("task");

        ADMIN_TASK.setUserId(USER2.getId());
        ADMIN_TASK.setName("Second");
        ADMIN_TASK.setStatus(Status.COMPLETED);
        ADMIN_TASK.setDescription("task");

        SESSION.setUserId(USER1.getId());
        SESSION.setRole(Role.USUAL);
        SESSION.setDate(new Date());

    }


}
