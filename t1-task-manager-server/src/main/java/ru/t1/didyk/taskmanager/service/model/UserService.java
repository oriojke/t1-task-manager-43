package ru.t1.didyk.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.dto.IProjectDtoRepository;
import ru.t1.didyk.taskmanager.api.repository.dto.ITaskDtoRepository;
import ru.t1.didyk.taskmanager.api.repository.model.IUserRepository;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.IPropertyService;
import ru.t1.didyk.taskmanager.api.service.model.IUserService;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.didyk.taskmanager.exception.field.*;
import ru.t1.didyk.taskmanager.model.User;
import ru.t1.didyk.taskmanager.repository.dto.ProjectDtoRepository;
import ru.t1.didyk.taskmanager.repository.dto.TaskDtoRepository;
import ru.t1.didyk.taskmanager.repository.model.UserRepository;
import ru.t1.didyk.taskmanager.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull IPropertyService propertyService, @NotNull IConnectionService connectionService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    protected @NotNull IUserRepository getRepository(@NotNull EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @NotNull
    public ITaskDtoRepository getTaskRepository(@NotNull EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @NotNull
    public IProjectDtoRepository getProjectRepository(@NotNull EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            entityManager.getTransaction().begin();
            @NotNull final User result = repository.add(user);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            @NotNull final User user = create(login, password);
            user.setEmail(email);
            entityManager.getTransaction().begin();
            @NotNull final User result = repository.add(user);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            @NotNull final User user = create(login, password);
            user.setRole(role);
            entityManager.getTransaction().begin();
            @NotNull final User result = repository.add(user);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            @Nullable final User user = repository.findById(id);
            return user;
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            @Nullable final User user = repository.findByLogin(login);
            return user;
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            @Nullable final User user = repository.findByEmail(email);
            return user;
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return removeOne(user);
    }

    @Nullable
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return remove(user);
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User updateUser(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.isLoginExists(login);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.isEmailExists(email);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final List<User> modelsRemove) {
        return;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            @Nullable final User user = findByLogin(login);
            entityManager.getTransaction().begin();
            user.setLocked(true);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            @Nullable final User user = findByLogin(login);
            entityManager.getTransaction().begin();
            user.setLocked(false);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User removeOne(@Nullable final User model) {
        if (model == null) return null;
        @Nullable final User user = super.remove(model);
        if (user == null) return null;
        @NotNull final String userId = user.getId();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            getTaskRepository(entityManager).removeAll(userId);
            getProjectRepository(entityManager).removeAll(userId);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}