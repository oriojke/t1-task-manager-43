package ru.t1.didyk.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.dto.IDtoRepository;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.dto.IDtoService;
import ru.t1.didyk.taskmanager.dto.model.AbstractModelDTO;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.exception.field.IdEmptyException;
import ru.t1.didyk.taskmanager.exception.field.IndexIncorrectException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDtoService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract IDtoRepository<M> getRepository(@NotNull final EntityManager entityManager);

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (comparator == null) return findAll();
            return repository.findAll(comparator);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M add(@Nullable final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (model == null) return null;
            entityManager.getTransaction().begin();
            @Nullable final M object = repository.add(model);
            entityManager.getTransaction().commit();
            return object;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (models.isEmpty()) return Collections.emptyList();
            entityManager.getTransaction().begin();
            @NotNull final Collection<M> objects = repository.add(models);
            entityManager.getTransaction().commit();
            return objects;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (models.isEmpty()) return Collections.emptyList();
            entityManager.getTransaction().begin();
            @NotNull final Collection<M> objects = repository.set(models);
            entityManager.getTransaction().commit();
            return objects;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (id == null || id.isEmpty()) return false;
            return repository.existsById(id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (id == null || id.isEmpty()) throw new IdEmptyException();
            return repository.findOneById(id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (index == null) throw new IndexIncorrectException();
            return repository.findOneByIndex(index);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.getSize();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (model == null) return null;
            entityManager.getTransaction().begin();
            @Nullable M object = repository.remove(model);
            entityManager.getTransaction().commit();
            return object;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (id == null || id.isEmpty()) throw new IdEmptyException();
            entityManager.getTransaction().begin();
            @Nullable M object = repository.removeById(id);
            entityManager.getTransaction().commit();
            return object;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (index == null) throw new IndexIncorrectException();
            entityManager.getTransaction().begin();
            @Nullable M object = repository.removeByIndex(index);
            entityManager.getTransaction().commit();
            return object;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            if (sort == null) return findAll();
            return repository.findAll(sort.getComparator());
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable List<M> modelsRemove) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            repository.removeAll(modelsRemove);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull M object) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            repository.update(object);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
