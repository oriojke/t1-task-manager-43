package ru.t1.didyk.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.model.IRepository;
import ru.t1.didyk.taskmanager.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public @Nullable M add(@Nullable M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    public @Nullable M remove(@Nullable M model) {
        entityManager.remove(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable List<M> modelsRemove) {
        modelsRemove.forEach(m -> remove(m));
    }

    @Override
    public @NotNull Collection<M> add(@NotNull Collection<M> models) {
        models.forEach(m -> add(m));
        return models;
    }

    @Override
    public @NotNull Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public void update(@NotNull M object) {
        entityManager.merge(object);
    }

    @Override
    public void clear() {
        findAll().forEach(m -> remove(m));
    }

    @Override
    public @NotNull List<M> findAll(@Nullable Comparator<M> comparator) {
        @NotNull final List<M> result = findAll();
        result.sort(comparator);
        return result;
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    public @Nullable M findOneByIndex(@Nullable Integer index) {
        return findAll().get(index);
    }

    @Override
    public int getSize() {
        return findAll().size();
    }

    @Override
    public @Nullable M removeById(@NotNull String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public @Nullable M removeByIndex(@NotNull Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

}
