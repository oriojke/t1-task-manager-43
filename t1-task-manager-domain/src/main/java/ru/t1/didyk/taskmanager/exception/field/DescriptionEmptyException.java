package ru.t1.didyk.taskmanager.exception.field;

public final class DescriptionEmptyException extends AbstractFieldException {

    public DescriptionEmptyException() {
        super("Error! Description is empty.");
    }

}
