package ru.t1.didyk.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "sessions")
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    @Column(name = "created")
    private Date date = new Date();

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
