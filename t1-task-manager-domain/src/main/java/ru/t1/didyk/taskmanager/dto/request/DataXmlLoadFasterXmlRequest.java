package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataXmlLoadFasterXmlRequest extends AbstractUserRequest {
    public DataXmlLoadFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
